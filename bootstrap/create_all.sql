USE [Dealership];
GO

DROP FUNCTION IF EXISTS [udf_CBU_GetAge];
DROP FUNCTION IF EXISTS [udf_CBU_Gender];
GO

CREATE FUNCTION DBO.[udf_CBU_GetAge](
    @Id_Number VARCHAR(13)
)
RETURNS INT
AS 
BEGIN
	DECLARE @Age INT;
	DECLARE @YearOfBirth INT;
	DECLARE @CurrentYear INT;

	SET @YearOfBirth = CAST(SUBSTRING(@Id_Number,1,2) AS INT);
	SET @CurrentYear = CAST(YEAR(GETDATE()) AS INT);

	IF @YearOfBirth > SUM(@CurrentYear - 2000)
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+1900);
    END;
    ELSE
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+2000);
    END;

	SET @Age = SUM(@CurrentYear- @YearOfBirth); 
    RETURN @Age;
END;
GO


CREATE FUNCTION DBO.[udf_CBU_Gender](
    @Id_Number VARCHAR(13)
)
RETURNS VARCHAR(6)
AS 
BEGIN
	DECLARE @GenderInt INT;
	DECLARE @Gender VARCHAR(6);

	SET @GenderInt = CAST(SUBSTRING(@Id_Number,7,4) AS INT);

	IF @GenderInt < 5000
    BEGIN
        SET @Gender = 'Female';
    END;
    ELSE
    BEGIN
        SET @Gender = 'Male';
    END;
 
    RETURN @Gender;
END;
GO

------ Examples 
SELECT DBO.[udf_CBU_GetAge]('6306034988038') AS Age;
SELECT DBO.[udf_CBU_GetAge]('8612059191789') AS Age;
GO

SELECT DBO.[udf_CBU_Gender]('6306034988038') AS Gender;
SELECT DBO.[udf_CBU_Gender]('8612059191789') AS Gender;
GO

































USE DealerShip
GO

DROP FUNCTION IF EXISTS udf_getListing;
DROP FUNCTION IF EXISTS udf_getListingsByStatus;
GO



CREATE FUNCTION udf_getListing
(   
    @vehicleId INT
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Email_Address,  Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
COALESCE(( SELECT STRING_AGG(Extras.[Name], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Name_Of_Extras,
COALESCE((SELECT STRING_AGG(Extras.[Description], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Description_Of_Extras,
COALESCE((SELECT STRING_AGG(Vehicle_Images.[Image_Id], ', ')
	FROM Vehicle_Images
WHERE  Vehicle_Images.[Vehicle_Id] = Listings.Vehicle_Id ), 'N/A') As List_Of_Images,

Vehicles.Colour, Vehicles.Engine_Capacity, Vehicles.Fuel_Type, Vehicles.Horsepower, Vehicles.Previous_Owners, Vehicles.Service_History,
Vehicles.Top_Speed, Vehicles.Wheel_Size, Vehicles.[Year], Addresses.Suburb, Addresses.Street_Address
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
WHERE Listings.Vehicle_Id = @vehicleId
);

GO
Select * from udf_getListing(26)
Select * from udf_getListing(44)

GO
CREATE FUNCTION udf_getListingsByStatus
(   
    @status Varchar(10)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
Business_Units.Email_Address, Addresses.Suburb, Addresses.Street_Address, Vehicle_Images.[Image], Packages.[Priority]
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
INNER JOIN Vehicle_Images ON Listings.Vehicle_Id = Vehicle_Images.Vehicle_Id
LEFT JOIN Packages ON Listings.Package_Id = Packages.Package_Id
WHERE Listings.Listing_Status = @status
);

GO
Select * from udf_getListingsByStatus('Sold')
Order by Priority desc;
GO


USE [Dealership]
GO

DROP FUNCTION IF EXISTS [udf_Split_String];
GO

CREATE FUNCTION [udf_Split_String]
    (
        @List NVARCHAR(MAX),
        @Delim VARCHAR(255)
    )
    RETURNS TABLE
    AS
        RETURN ( SELECT [Value], idx = RANK() OVER (ORDER BY n) FROM 
          ( 
            SELECT n = Number, 
              [Value] = LTRIM(RTRIM(SUBSTRING(@List, [Number],
              CHARINDEX(@Delim, @List + @Delim, [Number]) - [Number])))
            FROM (SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
              FROM sys.all_objects) AS x
              WHERE Number <= LEN(@List)
              AND SUBSTRING(@Delim + @List, [Number], LEN(@Delim)) = @Delim
          ) AS y
        );
GO




USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGender];
GO

CREATE VIEW [view_CBU_AgeGender]
AS
	SELECT  [BU_Id]
		,[First_Names]
		,[Last_Name]
		,[Id_Number]
		,DBO.[udf_CBU_GetAge](CBU.Id_Number) AS Age
		,DBO.[udf_CBU_Gender](CBU.Id_Number) AS Gender
	FROM [Customer_Business_Units] AS CBU;
GO


USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGenderDemographic];
GO

CREATE VIEW [view_CBU_AgeGenderDemographic]
AS
	SELECT [Gender] 
		,SUM( IIF( [Age] < 25, 1, 0 ) ) AS Under25
		,SUM( IIF( [Age] BETWEEN 25 AND 50, 1, 0 ) ) AS Between25_50
		,SUM( IIF( [Age] > 50, 1, 0 ) ) AS Over50
	FROM view_CBU_AgeGender
	GROUP BY [Gender];
GO


USE [Dealership]
GO

DROP VIEW IF EXISTS [view_CustomerBusinessUnits];
GO

CREATE VIEW [view_CustomerBusinessUnits] AS
	SELECT bu.[BU_Id], [First_Names], [Last_Name], [Phone_Number], [Email_Address], [Id_Number], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Customer_Business_Units] cbu
	INNER JOIN [Business_Units] bu ON cbu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example:
SELECT * FROM [view_CustomerBusinessUnits]
GO



DROP VIEW IF EXISTS CUSTOMERS_WITHOUT_LISTINGS;
DROP VIEW IF EXISTS CUSTOMERS_WITH_LISTINGS;
GO

CREATE VIEW CUSTOMERS_WITHOUT_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NULL;

GO

CREATE VIEW CUSTOMERS_WITH_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NOT NULL;

GO

SELECT * FROM CUSTOMERS_WITH_LISTINGS
ORDER BY BU_Id;

SELECT * FROM CUSTOMERS_WITHOUT_LISTINGS
ORDER BY BU_Id;
GO


USE [Dealership]
GO

DROP VIEW IF EXISTS [view_EnterpriseBusinessUnits];
GO


CREATE VIEW [view_EnterpriseBusinessUnits] AS
	SELECT bu.[BU_Id], ebu.[Enterprise_Name], ebu.[Enterprise_Number], bu.[Phone_Number], bu.[Email_Address], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Enterprise_Business_Units] ebu
	INNER JOIN [Business_Units] bu ON ebu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example
SELECT * FROM [view_EnterpriseBusinessUnits]
GO


DROP VIEW IF EXISTS [view_Manufacturers_Vehicles];
GO

CREATE VIEW view_Manufacturers_Vehicles
AS
	SELECT COUNT(V.Vehicle_Id) as [Total Vehicles], M.Name as [Manufacturer]  
	FROM Vehicles as V
	LEFT JOIN Manufacturers M
	ON M.Manufacturer_Id = V.Manufacturer_Id
	GROUP BY V.Manufacturer_Id,M.Name
GO


USE [Dealership]
GO


DROP VIEW IF EXISTS [view_SalesPerBusinessUnit];
GO


CREATE VIEW [view_SalesPerBusinessUnit] AS
	WITH listings_cte (BU_id, Total_Listings, Total_Price)
	AS
	(
		SELECT BU_Id, Count(*) AS Total_Listings, SUM(Price) AS Total_Price FROM Listings GROUP BY BU_Id
	)
	SELECT bu.[BU_Id],
	CASE
		WHEN bu.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN bu.[Customer_Type] = 0 THEN CONCAT(cbu.[First_Names], ' ', cbu.[Last_Name])
		ELSE ebu.[Enterprise_Name]
	END AS [Name],
	IsNull(l.[Total_Listings], 0) as [Total_Listings],
	IsNull(l.Total_Price, 0) as [Total_Price]
	FROM [Business_Units] bu
	LEFT JOIN [Customer_Business_Units] cbu ON cbu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN [Enterprise_Business_Units] ebu ON ebu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN listings_cte l on l.BU_id = bu.BU_Id
GO

-- Usage:
SELECT * FROM [view_SalesPerBusinessUnit]
ORDER BY [Total_Price] DESC
GO


DROP VIEW IF EXISTS [view_Vehicle_Totals];
GO

CREATE VIEW view_Vehicle_Totals
AS SELECT 
	(SELECT COUNT(*) FROM Bikes) AS Bikes,
	(SELECT COUNT(*) FROM CARS ) AS Cars,
	(SELECT COUNT(*) FROM Trucks) AS  Trucks
GO




USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_Addresses_Insert]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Select]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Update]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Delete]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_OrphanRecords]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_DeleteOrphanRecords]; 
GO

CREATE PROCEDURE [usp_Addresses_Insert]
	@Address_Id INT OUTPUT,
    @Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;
	
	INSERT INTO [Addresses] ([Street_Address], [Suburb], [PostalCode], [Country])
	SELECT @Street_Address, @Suburb, @PostalCode, @Country;
	
	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country]
	FROM   [Addresses]
	WHERE  [Address_Id] = SCOPE_IDENTITY();
	
	SET @Address_Id = SCOPE_IDENTITY();
	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Select] 
    @Address_Id INT
AS 
	SET NOCOUNT ON; 

	BEGIN TRANSACTION;

	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country] 
	FROM   [Addresses] 
	WHERE  [Address_Id] = @Address_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Update]
    @Address_Id INT,
    @Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30)
AS 
	SET NOCOUNT ON;  
	
	BEGIN TRANSACTION;

	UPDATE [Addresses]
	SET    [Street_Address] = @Street_Address, [Suburb] = @Suburb, [PostalCode] = @PostalCode, [Country] = @Country
	WHERE  [Address_Id] = @Address_Id;
	
	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country]
	FROM   [Addresses]
	WHERE  [Address_Id] = @Address_Id;	
	
	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Delete] 
    @Address_Id INT
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE
	FROM   [Addresses]
	WHERE  [Address_Id] = @Address_Id;

	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_OrphanRecords] 
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	SELECT	*
	FROM [Addresses] AS a
	LEFT JOIN [Business_Units] AS b ON a.Address_Id = b.Address_Id
	WHERE b.Address_Id IS NULL

	COMMIT;
GO

CREATE PROCEDURE [usp_Addresses_DeleteOrphanRecords] 
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE a
	FROM [Addresses] AS a
	LEFT JOIN [Business_Units] AS b ON a.Address_Id = b.Address_Id
	WHERE b.Address_Id IS NULL

	COMMIT;
GO

-------------------- Tests
-----------
DECLARE	@Address_Id int

EXEC	[dbo].[usp_Addresses_Insert]
		@Address_Id = @Address_Id OUTPUT,
		@Street_Address = N'50 Fisk Plaza',
		@Suburb = N'Dorp',
		@PostalCode = 8560,
		@Country = N'South Africa'

EXEC	[dbo].[usp_Addresses_Select]
		@Address_Id

EXEC	[dbo].[usp_Addresses_Update]
		@Address_Id,
		@Street_Address = N'75 Fisk Plaza',
		@Suburb = N'Dorpie',
		@PostalCode = 8560,
		@Country = N'South Africa'

EXEC	[dbo].[usp_Addresses_Delete]
		@Address_Id

EXEC	[dbo].[usp_Addresses_Select]
		@Address_Id

EXEC	[dbo].[usp_Addresses_OrphanRecords]

EXEC	[usp_Addresses_DeleteOrphanRecords]

EXEC	[dbo].[usp_Addresses_OrphanRecords]

GO


USE [Dealership]

DROP PROCEDURE IF EXISTS [usp_BU_Retrieve];
DROP PROCEDURE IF EXISTS [usp_BU_Insert];
DROP PROCEDURE IF EXISTS [usp_BU_Update];
GO


CREATE PROCEDURE [usp_BU_Retrieve]
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	SET XACT_ABORT ON

	BEGIN TRANSACTION;

	SELECT [BU_Id], [Phone_Number], [Email_Address], [Address_Id], [Customer_Type]
	FROM   [Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_BU_Insert] (
	@BU_Id int OUTPUT,
	@Phone_Number varchar(16),
	@Email_Address varchar(50),
	@Address_Id int,
	@Customer_Type smallint)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION

	INSERT INTO Business_Units([Phone_Number], [Email_Address], [Address_Id], [Customer_Type])
	VALUES (@Phone_Number, @Email_Address, @Address_Id, @Customer_Type)
	SET @BU_Id = SCOPE_IDENTITY();
	COMMIT TRANSACTION
GO


CREATE PROCEDURE [usp_BU_Update] (
	@BU_Id int,
	@Phone_Number varchar(16),
	@Email_Address varchar(50))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	UPDATE Business_Units
	SET [Phone_Number] = @Phone_Number,
		[Email_Address] = @Email_Address
	WHERE [BU_Id] = @BU_Id

	COMMIT
GO


USE [Dealership]

DROP PROCEDURE IF EXISTS [usp_CBU_Insert];
DROP PROCEDURE IF EXISTS [usp_CBU_Retrieve];
DROP PROCEDURE IF EXISTS [usp_CBU_Update];
GO


CREATE PROCEDURE [usp_CBU_Insert] (
	@BU_Id int OUTPUT,
	@Phone_Number varchar(16),
	@Email_Address varchar(50),
	@First_Names varchar(255),
	@Last_Name varchar(255),
	@Id_Number varchar(13),
	@Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	DECLARE @Customer_Type int = 0 -- Customer
	DECLARE @Address_Id int

	EXECUTE usp_Addresses_Insert @Address_Id OUTPUT, @Street_Address, @Suburb, @PostalCode, @Country
	EXECUTE usp_BU_Insert @BU_Id OUTPUT, @Phone_Number, @Email_Address, @Address_Id, @Customer_Type
	
	INSERT INTO Customer_Business_Units([BU_Id], [First_Names], [Last_Name], [Id_Number])
	VALUES(@BU_Id, @First_Names, @Last_Name, @Id_Number)

	COMMIT
GO

-- Example
DECLARE @BU_Id int
EXECUTE usp_CBU_Insert
@BU_Id OUTPUT,
@Phone_Number='0768464875',
@Email_Address='john@gmail.com',
@First_Names='John',
@Last_Name='Bread',
@Id_Number='7811080719547',
@Street_Address='807 Bezuidenhout St',
@Suburb='Secunda',
@PostalCode=2301,
@Country='South Africa'
SELECT @BU_Id as BU_Id
GO

/*
EXECUTE [usp_CBU_Retrieve] 103
GO
*/

SELECT * FROM view_CustomerBusinessUnits
WHERE BU_Id = 103
GO

CREATE PROCEDURE [usp_CBU_Retrieve] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	SET XACT_ABORT ON

	BEGIN TRANSACTION;

	SELECT [BU_Id], [First_Names], [Last_Name], [Id_Number]
	FROM   [Customer_Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO

EXECUTE [usp_CBU_Retrieve] 45
GO

CREATE PROCEDURE [usp_CBU_Update] (
	@BU_Id int,
	@First_Names varchar(255),
	@Last_Name varchar(255),
	@Id_Number varchar(13))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	UPDATE Customer_Business_Units
	SET [First_Names] = IsNull(@First_Names, [First_Names]),
		[Last_Name] = IsNull(@Last_Name, [Last_Name]),
		[Id_Number] = IsNull(@Id_Number, [Id_Number])
	WHERE [BU_Id] = @BU_Id
	COMMIT
GO

EXECUTE [usp_CBU_Retrieve] 45
GO

EXECUTE [usp_CBU_Update] @BU_Id = 45, @First_Names=NULL, @Last_Name='Lewis', @Id_Number=NULL
GO

EXECUTE [usp_CBU_Retrieve] 45
GO


USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_EBU_Insert];
DROP PROCEDURE IF EXISTS [usp_EBU_Retrieve]; 
DROP PROCEDURE IF EXISTS [usp_EBU_Update]; 
DROP PROCEDURE IF EXISTS [usp_EBU_Delete]; 
GO

CREATE PROCEDURE [usp_EBU_Insert] 
	@BU_Id INT ,
    @Enterprise_Name VARCHAR(30),
    @Enterprise_Number VARCHAR(14)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	INSERT INTO [Enterprise_Business_Units] ([BU_Id], [Enterprise_Name], [Enterprise_Number])
	SELECT @BU_Id, @Enterprise_Name, @Enterprise_Number;
	
	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number]
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;
	           
	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Retrieve] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 

	BEGIN TRANSACTION;

	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number] 
	FROM   [Enterprise_Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Update] 
    @BU_Id INT,
    @Enterprise_Name VARCHAR(30),
    @Enterprise_Number VARCHAR(14)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	UPDATE [Enterprise_Business_Units]
	SET    [Enterprise_Name] = @Enterprise_Name, [Enterprise_Number] = @Enterprise_Number
	WHERE  [BU_Id] = @BU_Id;
	
	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number]
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;	
	
	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Delete] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;

	COMMIT;
GO


-------------------- Tests
----------
DECLARE	@BU_Id int
SET @BU_Id = 100

EXEC	[dbo].[usp_EBU_Retrieve]
		@BU_Id

EXEC	[dbo].[usp_EBU_Delete]
		@BU_Id

EXEC	[dbo].[usp_EBU_Retrieve]
		@BU_Id

EXEC	[dbo].[usp_EBU_Insert]
		@BU_Id,
		@Enterprise_Name = N'BBD',
		@Enterprise_Number = N'B1234567891234'

EXEC	[dbo].[usp_EBU_Update]
		@BU_Id,
		@Enterprise_Name = N'Kazu',
		@Enterprise_Number = N'C5579367520489'

GO


USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Manufacturer_Vehicles_Uploaded];
GO
CREATE PROCEDURE [usp_Monthly_Manufacturer_Vehicles_Uploaded]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())		
SELECT DISTINCT mfr.Name, COUNT(lst.Listing_Id) OVER (PARTITION BY mfr.Manufacturer_Id) AS 'Vehicles Uploaded'
FROM Listings AS lst 
INNER JOIN Vehicles AS vhc
	ON lst.Vehicle_Id = vhc.Vehicle_Id
LEFT JOIN Manufacturers AS mfr
	ON vhc.Manufacturer_Id = mfr.Manufacturer_Id
WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year		
ORDER BY 'Vehicles Uploaded' DESC;
GO

USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Package_Popularity];
GO

CREATE PROCEDURE [usp_Monthly_Package_Popularity]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())
	SELECT DISTINCT pkg.Name, pkg.Description,
					pkg.Price,
					COUNT(lst.Package_Id) OVER (PARTITION BY lst.Package_Id) AS 'Number of Listings',
					COUNT(lst.Package_Id) OVER (PARTITION BY lst.Package_Id) * pkg.Price AS Revenue
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
    WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year
	ORDER BY 'Number of Listings' DESC;
GO

USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Total_Revenue];
GO

CREATE PROCEDURE [usp_Monthly_Total_Revenue]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())
	SELECT  SUM(pkg.Price) AS 'Month Revenue'
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
    WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year ;	
GO

USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Total_Revenue_Summary];
GO

CREATE PROCEDURE [usp_Monthly_Total_Revenue_Summary]
	@Year int = null
AS
	IF @Year is null
		SET @Year = YEAR(GETDATE())	
	SELECT DISTINCT DATENAME(month, lst.Date_Created) AS Month,
			SUM(pkg.Price) OVER (PARTITION BY MONTH(lst.Date_Created)) AS 'Total Revenue'
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
	WHERE YEAR(lst.Date_Created) = @Year ;	    
GO

USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Vehicle_Create];
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [usp_Vehicle_Create](
@Model varchar(20),
@Year int,
@Colour varchar(20),
@Engine_Capacity decimal(4,2),
@Wheel_Size tinyint,
@Mileage int = 0,
@Fuel_Type bit,
@Top_Speed smallint,
@Previous_Owners int = 0,
@Service_History varchar(20),
@Horsepower smallint,
@Vehicle_Vin char(17),
@Automatic_Transmission bit,
@Manufacturer varchar(20),
@Image varbinary(max) = NULL,
@VehicleType int,
@ExtrasList varchar(100) = NULL,
@Type varchar(20) = NULL,
@Carry_Capacity smallint = NULL,
@Shape varchar(20) = NULL,
@Doors tinyint = NULL,
@Body_Type varchar(20) = NULL)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
		
		IF @VehicleType NOT IN (1, 2, 3)
		BEGIN
			RAISERROR('Invaild Vehicle Type',16,1);
		END

		DECLARE @ManufacturerId int;
		DECLARE @FuelType varchar(20) = 'Petrol'
		DECLARE @VehicleId int;

		DECLARE @extras TABLE (
		Extra_Id INT NOT NULL,
		Vehicle_Id Int NOT NULL);

		SET @ManufacturerId = (SELECT Manufacturer_Id FROM Manufacturers
		WHERE Name = @Manufacturer);
	
		IF @ManufacturerId IS NULL
		BEGIN 
			INSERT INTO Manufacturers
			(
				Name,
				Email,
				Phone_Number
			)
			VALUES 
			(
				@Manufacturer,
				@Manufacturer+'@company.com', --validate Email Function
				'+27145787894'
			)

			SET @ManufacturerId = SCOPE_IDENTITY();

			PRINT 'COMMIT : INSERT INTO Manufacturers'

		END
	
		IF @Fuel_Type = 1
		BEGIN
			SET @FuelType = 'Diesel'
		END
	
		
		INSERT INTO Vehicles
		(
			Model,
			Year,
			Colour,
			Engine_Capacity,
			Wheel_Size,
			Mileage,
			Fuel_Type,
			Top_Speed,
			Previous_Owners,
			Service_History,
			Horsepower,
			Vehicle_Vin,
			Automatic_Transmission,
			Manufacturer_Id
		)
		VALUES
		(
			@Model,
			@Year,
			@Colour,
			@Engine_Capacity,
			@Wheel_Size,
			@Mileage,
			@FuelType,
			@Top_Speed,
			@Previous_Owners,
			@Service_History,
			@Horsepower,
			@Vehicle_Vin,
			@Automatic_Transmission,
			@ManufacturerId
		)

		PRINT 'COMMIT : INSERT INTO Vehicles'

		SELECT @VehicleId = SCOPE_IDENTITY();

		--CAR INSERT
		IF @VehicleType = 1
		BEGIN
		
			INSERT INTO Cars
			(
				Vehicle_Id,
				Shape,
				Doors
			)
			VALUES
			(
				@VehicleId,
				@Shape,
				@Doors
			)
			
			PRINT 'COMMIT : INSERT INTO Cars'
			
		END

		--BIKE INSERT
		IF @VehicleType = 2
		BEGIN

			INSERT INTO Bikes
			(
				Vehicle_Id,
				Body_Type
			)
			VALUES
			(
				@VehicleId,
				@Body_Type
			)
				
			PRINT 'COMMIT : INSERT INTO Bikes'

		END

		--TRUCK INSERT
		IF @VehicleType = 3
		BEGIN
			
			INSERT INTO Trucks
			(
				Vehicle_Id,
				Type,
				Carry_Capacity
			)
			VALUES
			(
				@VehicleId,
				@Type,
				@Carry_Capacity
			)
			
			PRINT 'COMMIT : INSERT INTO Trucks'
			
		END
		

		IF @Image IS NULL
		BEGIN
			SET @Image = (Select Image from Vehicle_Images where Image_Id = 1);
		END

		INSERT INTO Vehicle_Images
		(
			Vehicle_Id,
			Image
		)
		VALUES
		(
			@VehicleId,
			@Image
		)

		PRINT 'COMMIT : INSERT INTO Image'
		
		IF @ExtrasList IS NOT NULL
		BEGIN

			INSERT INTO Vehicle_Extras
			SELECT Value as Extra_Id, @VehicleId as Vehicle_Id
			from udf_Split_String(@ExtrasList,',')

			PRINT 'COMMIT : INSERT INTO Vehicle_Extras'
			
		END

		COMMIT TRANSACTION 
		print 'Transaction committed : Added Vehicle ' + CAST(@VehicleId AS VARCHAR)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT 'ERROR : ' + ERROR_MESSAGE()
	END CATCH

END
GO


USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Vehicle_Update];
GO

CREATE PROCEDURE [usp_Vehicle_Update](
@Vehicle_Id int,
@Model varchar(20),
@Year int,
@Colour varchar(20),
@Engine_Capacity decimal(4,2),
@Wheel_Size tinyint,
@Mileage int = 0,
@Fuel_Type bit,
@Top_Speed smallint,
@Previous_Owners int = 0,
@Service_History varchar(20),
@Horsepower smallint,
@Vehicle_Vin char(17),
@Automatic_Transmission bit,
@Manufacturer varchar(20),
@Image varbinary(max) = NULL,
@ExtrasList varchar(100) = NULL,
@Type varchar(20) = NULL,
@Carry_Capacity smallint = NULL,
@Shape varchar(20) = NULL,
@Doors tinyint = NULL,
@Body_Type varchar(20) = NULL)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
		
		DECLARE @ManufacturerId int;
		DECLARE @FuelType varchar(20) = 'Petrol'
		DECLARE @VehicleType int = 0;

		IF EXISTS(SELECT * FROM Cars WHERE Vehicle_Id = @Vehicle_Id)
		BEGIN
			SET @VehicleType = 1;
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT * FROM Bikes WHERE Vehicle_Id = @Vehicle_Id)
			BEGIN
				SET @VehicleType = 2;
			END
			ELSE
			BEGIN
				IF EXISTS(SELECT * FROM Trucks WHERE Vehicle_Id = @Vehicle_Id)
				BEGIN
					SET @VehicleType = 3;
				END
			END
		END

		IF @VehicleType NOT IN (1, 2, 3)
		BEGIN
			RAISERROR('Invaild Vehicle Type',16,1);
		END

		SET @ManufacturerId = (SELECT Manufacturer_Id FROM Manufacturers WHERE Name = @Manufacturer);
	
		IF @ManufacturerId IS NULL
		BEGIN 
			INSERT INTO Manufacturers
			(
				Name,
				Email,
				Phone_Number
			)
			VALUES 
			(
				@Manufacturer,
				@Manufacturer+'@company.com',
				'+27145787894'
			)

			SET @ManufacturerId = SCOPE_IDENTITY();

			PRINT 'COMMIT : INSERT INTO Manufacturers'
		END
	
		IF @Fuel_Type = 1
		BEGIN
			SET @FuelType = 'Diesel'
		END
	
		UPDATE Vehicles
		SET Model = @Model,
		Year = @Year,
		Engine_Capacity = @Engine_Capacity,
		Wheel_Size = @Wheel_Size,
		Mileage = @Mileage,
		Fuel_Type = @FuelType,
		Top_Speed = @Top_Speed,
		Previous_Owners = @Previous_Owners,
		Service_History = @Service_History,
		Horsepower = @Horsepower,
		Vehicle_Vin = @Vehicle_Vin,
		Automatic_Transmission = @Automatic_Transmission,
		Manufacturer_Id = @ManufacturerId
		WHERE
		Vehicle_Id = @Vehicle_Id
		
		PRINT 'COMMIT : UPDATE INTO Vehicles'

		--CAR UPDATE
		IF @VehicleType = 1
		BEGIN

			UPDATE Cars
			SET Shape = @Shape,
			Doors = @Doors
			WHERE
			Vehicle_Id = @Vehicle_Id
			
			PRINT 'COMMIT : UPDATE INTO Cars'
			
		END

		--BIKE UPDATE
		IF @VehicleType = 2
		BEGIN

			UPDATE Bikes
			SET Body_Type = Body_Type
			WHERE
			Vehicle_Id = @Vehicle_Id
				
			PRINT 'COMMIT : UPDATE INTO Bikes'

		END

		--TRUCK UPDATE
		IF @VehicleType = 3
		BEGIN
			
			UPDATE Trucks
			SET Type = @Type,
			Carry_Capacity = @Carry_Capacity
			WHERE
			Vehicle_Id = @Vehicle_Id
			
			PRINT 'COMMIT : UPDATE INTO Trucks'
			
		END
		

		IF @Image IS NULL
		BEGIN
			SET @Image = (Select Image from Vehicle_Images where Image_Id = 1);
		END

		UPDATE Vehicle_Images
		SET Image = @Image
		WHERE
		Vehicle_Id = @Vehicle_Id

		PRINT 'COMMIT : UPDATE INTO Image'

		IF @ExtrasList IS NOT NULL
		BEGIN

			DELETE FROM Vehicle_Extras
			WHERE Vehicle_Id = @Vehicle_Id

			INSERT INTO Vehicle_Extras
			SELECT Value as Extra_Id, @Vehicle_Id as Vehicle_Id
			from udf_Split_String(@ExtrasList,',')

			PRINT 'COMMIT : UPDATE INTO Vehicle_Extras'
			
		END

		COMMIT TRANSACTION 
		print 'Transaction committed : Updated Vehicle ' + CAST(@Vehicle_Id AS VARCHAR)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT 'ERROR : ' + ERROR_MESSAGE()
	END CATCH

END
GO




