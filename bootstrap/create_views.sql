USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGender];
GO

CREATE VIEW [view_CBU_AgeGender]
AS
	SELECT  [BU_Id]
		,[First_Names]
		,[Last_Name]
		,[Id_Number]
		,DBO.[udf_CBU_GetAge](CBU.Id_Number) AS Age
		,DBO.[udf_CBU_Gender](CBU.Id_Number) AS Gender
	FROM [Customer_Business_Units] AS CBU;
GO


USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGenderDemographic];
GO

CREATE VIEW [view_CBU_AgeGenderDemographic]
AS
	SELECT [Gender] 
		,SUM( IIF( [Age] < 25, 1, 0 ) ) AS Under25
		,SUM( IIF( [Age] BETWEEN 25 AND 50, 1, 0 ) ) AS Between25_50
		,SUM( IIF( [Age] > 50, 1, 0 ) ) AS Over50
	FROM view_CBU_AgeGender
	GROUP BY [Gender];
GO


USE [Dealership]
GO

DROP VIEW IF EXISTS [view_CustomerBusinessUnits];
GO

CREATE VIEW [view_CustomerBusinessUnits] AS
	SELECT bu.[BU_Id], [First_Names], [Last_Name], [Phone_Number], [Email_Address], [Id_Number], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Customer_Business_Units] cbu
	INNER JOIN [Business_Units] bu ON cbu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example:
SELECT * FROM [view_CustomerBusinessUnits]
GO



DROP VIEW IF EXISTS CUSTOMERS_WITHOUT_LISTINGS;
DROP VIEW IF EXISTS CUSTOMERS_WITH_LISTINGS;
GO

CREATE VIEW CUSTOMERS_WITHOUT_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NULL;

GO

CREATE VIEW CUSTOMERS_WITH_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NOT NULL;

GO

SELECT * FROM CUSTOMERS_WITH_LISTINGS
ORDER BY BU_Id;

SELECT * FROM CUSTOMERS_WITHOUT_LISTINGS
ORDER BY BU_Id;
GO


USE [Dealership]
GO

DROP VIEW IF EXISTS [view_EnterpriseBusinessUnits];
GO


CREATE VIEW [view_EnterpriseBusinessUnits] AS
	SELECT bu.[BU_Id], ebu.[Enterprise_Name], ebu.[Enterprise_Number], bu.[Phone_Number], bu.[Email_Address], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Enterprise_Business_Units] ebu
	INNER JOIN [Business_Units] bu ON ebu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example
SELECT * FROM [view_EnterpriseBusinessUnits]
GO


DROP VIEW IF EXISTS [view_Manufacturers_Vehicles];
GO

CREATE VIEW view_Manufacturers_Vehicles
AS
	SELECT COUNT(V.Vehicle_Id) as [Total Vehicles], M.Name as [Manufacturer]  
	FROM Vehicles as V
	LEFT JOIN Manufacturers M
	ON M.Manufacturer_Id = V.Manufacturer_Id
	GROUP BY V.Manufacturer_Id,M.Name
GO


USE [Dealership]
GO


DROP VIEW IF EXISTS [view_SalesPerBusinessUnit];
GO


CREATE VIEW [view_SalesPerBusinessUnit] AS
	WITH listings_cte (BU_id, Total_Listings, Total_Price)
	AS
	(
		SELECT BU_Id, Count(*) AS Total_Listings, SUM(Price) AS Total_Price FROM Listings GROUP BY BU_Id
	)
	SELECT bu.[BU_Id],
	CASE
		WHEN bu.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN bu.[Customer_Type] = 0 THEN CONCAT(cbu.[First_Names], ' ', cbu.[Last_Name])
		ELSE ebu.[Enterprise_Name]
	END AS [Name],
	IsNull(l.[Total_Listings], 0) as [Total_Listings],
	IsNull(l.Total_Price, 0) as [Total_Price]
	FROM [Business_Units] bu
	LEFT JOIN [Customer_Business_Units] cbu ON cbu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN [Enterprise_Business_Units] ebu ON ebu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN listings_cte l on l.BU_id = bu.BU_Id
GO

-- Usage:
SELECT * FROM [view_SalesPerBusinessUnit]
ORDER BY [Total_Price] DESC
GO


DROP VIEW IF EXISTS [view_Vehicle_Totals];
GO

CREATE VIEW view_Vehicle_Totals
AS SELECT 
	(SELECT COUNT(*) FROM Bikes) AS Bikes,
	(SELECT COUNT(*) FROM CARS ) AS Cars,
	(SELECT COUNT(*) FROM Trucks) AS  Trucks
GO


