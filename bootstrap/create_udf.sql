USE [Dealership];
GO

DROP FUNCTION IF EXISTS [udf_CBU_GetAge];
DROP FUNCTION IF EXISTS [udf_CBU_Gender];
GO

CREATE FUNCTION DBO.[udf_CBU_GetAge](
    @Id_Number VARCHAR(13)
)
RETURNS INT
AS 
BEGIN
	DECLARE @Age INT;
	DECLARE @YearOfBirth INT;
	DECLARE @CurrentYear INT;

	SET @YearOfBirth = CAST(SUBSTRING(@Id_Number,1,2) AS INT);
	SET @CurrentYear = CAST(YEAR(GETDATE()) AS INT);

	IF @YearOfBirth > SUM(@CurrentYear - 2000)
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+1900);
    END;
    ELSE
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+2000);
    END;

	SET @Age = SUM(@CurrentYear- @YearOfBirth); 
    RETURN @Age;
END;
GO


CREATE FUNCTION DBO.[udf_CBU_Gender](
    @Id_Number VARCHAR(13)
)
RETURNS VARCHAR(6)
AS 
BEGIN
	DECLARE @GenderInt INT;
	DECLARE @Gender VARCHAR(6);

	SET @GenderInt = CAST(SUBSTRING(@Id_Number,7,4) AS INT);

	IF @GenderInt < 5000
    BEGIN
        SET @Gender = 'Female';
    END;
    ELSE
    BEGIN
        SET @Gender = 'Male';
    END;
 
    RETURN @Gender;
END;
GO

------ Examples 
SELECT DBO.[udf_CBU_GetAge]('6306034988038') AS Age;
SELECT DBO.[udf_CBU_GetAge]('8612059191789') AS Age;
GO

SELECT DBO.[udf_CBU_Gender]('6306034988038') AS Gender;
SELECT DBO.[udf_CBU_Gender]('8612059191789') AS Gender;
GO

































USE DealerShip
GO

DROP FUNCTION IF EXISTS udf_getListing;
DROP FUNCTION IF EXISTS udf_getListingsByStatus;
GO



CREATE FUNCTION udf_getListing
(   
    @vehicleId INT
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Email_Address,  Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
COALESCE(( SELECT STRING_AGG(Extras.[Name], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Name_Of_Extras,
COALESCE((SELECT STRING_AGG(Extras.[Description], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Description_Of_Extras,
COALESCE((SELECT STRING_AGG(Vehicle_Images.[Image_Id], ', ')
	FROM Vehicle_Images
WHERE  Vehicle_Images.[Vehicle_Id] = Listings.Vehicle_Id ), 'N/A') As List_Of_Images,

Vehicles.Colour, Vehicles.Engine_Capacity, Vehicles.Fuel_Type, Vehicles.Horsepower, Vehicles.Previous_Owners, Vehicles.Service_History,
Vehicles.Top_Speed, Vehicles.Wheel_Size, Vehicles.[Year], Addresses.Suburb, Addresses.Street_Address
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
WHERE Listings.Vehicle_Id = @vehicleId
);

GO
Select * from udf_getListing(26)
Select * from udf_getListing(44)

GO
CREATE FUNCTION udf_getListingsByStatus
(   
    @status Varchar(10)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
Business_Units.Email_Address, Addresses.Suburb, Addresses.Street_Address, Vehicle_Images.[Image], Packages.[Priority]
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
INNER JOIN Vehicle_Images ON Listings.Vehicle_Id = Vehicle_Images.Vehicle_Id
LEFT JOIN Packages ON Listings.Package_Id = Packages.Package_Id
WHERE Listings.Listing_Status = @status
);

GO
Select * from udf_getListingsByStatus('Sold')
Order by Priority desc;
GO


USE [Dealership]
GO

DROP FUNCTION IF EXISTS [udf_Split_String];
GO

CREATE FUNCTION [udf_Split_String]
    (
        @List NVARCHAR(MAX),
        @Delim VARCHAR(255)
    )
    RETURNS TABLE
    AS
        RETURN ( SELECT [Value], idx = RANK() OVER (ORDER BY n) FROM 
          ( 
            SELECT n = Number, 
              [Value] = LTRIM(RTRIM(SUBSTRING(@List, [Number],
              CHARINDEX(@Delim, @List + @Delim, [Number]) - [Number])))
            FROM (SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
              FROM sys.all_objects) AS x
              WHERE Number <= LEN(@List)
              AND SUBSTRING(@Delim + @List, [Number], LEN(@Delim)) = @Delim
          ) AS y
        );
GO


