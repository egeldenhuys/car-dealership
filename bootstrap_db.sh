#!/bin/bash

convert_to_utf8() {
    iconv -f $(file -b --mime-encoding "$1") -t utf-8 < "$1" >> "$2"
    echo -e "\n" >> "$2"
}

set -x

bootstrap_dir="bootstrap"
mkdir -p $bootstrap_dir

proc_dir="User Stored Procedures"
proc_output_file="$bootstrap_dir/create_procs.sql"
echo -n "" > $proc_output_file
truncate --size 0 $proc_output_file

convert_to_utf8 "$proc_dir/usp_Addresses_CRUD.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_BusinessUnits_CRUD.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_CustomerBusinessUnits_CRUD.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_EnterpriseBusinessUnits_CRUD.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Monthly_Manufacturer_Vehicles_Uploaded.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Monthly_Package_Popularity.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Monthly_Total_Revenue.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Monthly_Total_Revenue_Summary.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Vehicle_Create.sql" $proc_output_file
convert_to_utf8 "$proc_dir/usp_Vehicle_Update.sql" $proc_output_file

udf_dir="User Defined Functions"
udf_output_file="$bootstrap_dir/create_udf.sql"
echo -n "" > $udf_output_file
truncate --size 0 $udf_output_file

convert_to_utf8 "$udf_dir/udf_Customer_Tables.sql" $udf_output_file
convert_to_utf8 "$udf_dir/udf_Listings.sql" $udf_output_file
convert_to_utf8 "$udf_dir/udf_Split_String.sql" $udf_output_file

view_dir="Views"
view_output_file="$bootstrap_dir/create_views.sql"
echo -n "" > $view_output_file
truncate --size 0 $view_output_file

convert_to_utf8 "$view_dir/view_CBU_AgeGender.sql" $view_output_file
convert_to_utf8 "$view_dir/view_CBU_AgeGenderDemographic.sql" $view_output_file
convert_to_utf8 "$view_dir/view_CustomerBusinessUnits.sql" $view_output_file
convert_to_utf8 "$view_dir/view_customers_and_listings.sql" $view_output_file
convert_to_utf8 "$view_dir/view_EnterpriseBusinessUnits.sql" $view_output_file
convert_to_utf8 "$view_dir/view_Manufacturers_Vehicles.sql" $view_output_file
convert_to_utf8 "$view_dir/view_SalesPerBusinessUnit.sql" $view_output_file
convert_to_utf8 "$view_dir/view_Vehicle_Totals.sql" $view_output_file

create_all_file="$bootstrap_dir/create_all.sql"

echo -n "" $create_all_file
truncate --size 0 $create_all_file

convert_to_utf8 $udf_output_file $create_all_file
convert_to_utf8 $view_output_file $create_all_file
convert_to_utf8 $proc_output_file $create_all_file

sed -i $create_all_file -e 's/\xef\xbb\xbf/\n/g'  

echo "DONE"
