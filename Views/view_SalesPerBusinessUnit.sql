USE [Dealership]
GO


DROP VIEW IF EXISTS [view_SalesPerBusinessUnit];
GO


CREATE VIEW [view_SalesPerBusinessUnit] AS
	WITH listings_cte (BU_id, Total_Listings, Total_Price)
	AS
	(
		SELECT BU_Id, Count(*) AS Total_Listings, SUM(Price) AS Total_Price FROM Listings GROUP BY BU_Id
	)
	SELECT bu.[BU_Id],
	CASE
		WHEN bu.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN bu.[Customer_Type] = 0 THEN CONCAT(cbu.[First_Names], ' ', cbu.[Last_Name])
		ELSE ebu.[Enterprise_Name]
	END AS [Name],
	IsNull(l.[Total_Listings], 0) as [Total_Listings],
	IsNull(l.Total_Price, 0) as [Total_Price]
	FROM [Business_Units] bu
	LEFT JOIN [Customer_Business_Units] cbu ON cbu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN [Enterprise_Business_Units] ebu ON ebu.[BU_Id] = bu.[BU_Id]
	LEFT JOIN listings_cte l on l.BU_id = bu.BU_Id
GO

-- Usage:
SELECT * FROM [view_SalesPerBusinessUnit]
ORDER BY [Total_Price] DESC
GO
