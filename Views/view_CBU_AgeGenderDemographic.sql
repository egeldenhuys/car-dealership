USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGenderDemographic];
GO

CREATE VIEW [view_CBU_AgeGenderDemographic]
AS
	SELECT [Gender] 
		,SUM( IIF( [Age] < 25, 1, 0 ) ) AS Under25
		,SUM( IIF( [Age] BETWEEN 25 AND 50, 1, 0 ) ) AS Between25_50
		,SUM( IIF( [Age] > 50, 1, 0 ) ) AS Over50
	FROM view_CBU_AgeGender
	GROUP BY [Gender];
GO
