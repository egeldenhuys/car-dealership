DROP VIEW IF EXISTS [view_Vehicle_Totals];
GO

CREATE VIEW view_Vehicle_Totals
AS SELECT 
	(SELECT COUNT(*) FROM Bikes) AS Bikes,
	(SELECT COUNT(*) FROM CARS ) AS Cars,
	(SELECT COUNT(*) FROM Trucks) AS  Trucks
GO
