USE [Dealership];
GO

DROP VIEW IF EXISTS [view_CBU_AgeGender];
GO

CREATE VIEW [view_CBU_AgeGender]
AS
	SELECT  [BU_Id]
		,[First_Names]
		,[Last_Name]
		,[Id_Number]
		,DBO.[udf_CBU_GetAge](CBU.Id_Number) AS Age
		,DBO.[udf_CBU_Gender](CBU.Id_Number) AS Gender
	FROM [Customer_Business_Units] AS CBU;
GO
