
DROP VIEW IF EXISTS CUSTOMERS_WITHOUT_LISTINGS;
DROP VIEW IF EXISTS CUSTOMERS_WITH_LISTINGS;
GO

CREATE VIEW CUSTOMERS_WITHOUT_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NULL;

GO

CREATE VIEW CUSTOMERS_WITH_LISTINGS AS 
SELECT Business_Units.[BU_Id],
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN 'Private'
		ELSE 'Enterprise'
	END AS Customer_Type,
	CASE
		WHEN Business_Units.[Customer_Type] = 0 THEN CONCAT([Customer_Business_Units].[First_Names], ' ', [Customer_Business_Units].[Last_Name])
		ELSE [Enterprise_Business_Units].[Enterprise_Name]
	END AS [Name],
	Business_Units.Email_Address,
	Business_Units.Phone_Number
FROM [Business_Units]
LEFT JOIN [Customer_Business_Units] ON [Customer_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN [Enterprise_Business_Units] ON [Enterprise_Business_Units].[BU_Id] = Business_Units.[BU_Id]
LEFT JOIN Listings ON Listings.BU_Id = Business_Units.BU_Id
WHERE Listings.BU_Id IS NOT NULL;

GO

SELECT * FROM CUSTOMERS_WITH_LISTINGS
ORDER BY BU_Id;

SELECT * FROM CUSTOMERS_WITHOUT_LISTINGS
ORDER BY BU_Id;
GO
