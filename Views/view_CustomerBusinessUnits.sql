USE [Dealership]
GO

DROP VIEW IF EXISTS [view_CustomerBusinessUnits];
GO

CREATE VIEW [view_CustomerBusinessUnits] AS
	SELECT bu.[BU_Id], [First_Names], [Last_Name], [Phone_Number], [Email_Address], [Id_Number], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Customer_Business_Units] cbu
	INNER JOIN [Business_Units] bu ON cbu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example:
SELECT * FROM [view_CustomerBusinessUnits]
GO
