USE [Dealership]
GO

DROP VIEW IF EXISTS [view_EnterpriseBusinessUnits];
GO


CREATE VIEW [view_EnterpriseBusinessUnits] AS
	SELECT bu.[BU_Id], ebu.[Enterprise_Name], ebu.[Enterprise_Number], bu.[Phone_Number], bu.[Email_Address], addr.[Street_Address], addr.[Suburb], addr.[PostalCode], addr.[Country]
	FROM [Enterprise_Business_Units] ebu
	INNER JOIN [Business_Units] bu ON ebu.[BU_Id] = bu.[BU_Id]
	INNER JOIN [Addresses] addr ON bu.Address_Id = addr.Address_Id
GO

-- Example
SELECT * FROM [view_EnterpriseBusinessUnits]
GO
