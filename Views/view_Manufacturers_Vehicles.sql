DROP VIEW IF EXISTS [view_Manufacturers_Vehicles];
GO

CREATE VIEW view_Manufacturers_Vehicles
AS
	SELECT COUNT(V.Vehicle_Id) as [Total Vehicles], M.Name as [Manufacturer]  
	FROM Vehicles as V
	LEFT JOIN Manufacturers M
	ON M.Manufacturer_Id = V.Manufacturer_Id
	GROUP BY V.Manufacturer_Id,M.Name
GO
