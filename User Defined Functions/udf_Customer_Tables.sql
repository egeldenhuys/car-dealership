USE [Dealership];
GO

DROP FUNCTION IF EXISTS [udf_CBU_GetAge];
DROP FUNCTION IF EXISTS [udf_CBU_Gender];
GO

CREATE FUNCTION DBO.[udf_CBU_GetAge](
    @Id_Number VARCHAR(13)
)
RETURNS INT
AS 
BEGIN
	DECLARE @Age INT;
	DECLARE @YearOfBirth INT;
	DECLARE @CurrentYear INT;

	SET @YearOfBirth = CAST(SUBSTRING(@Id_Number,1,2) AS INT);
	SET @CurrentYear = CAST(YEAR(GETDATE()) AS INT);

	IF @YearOfBirth > SUM(@CurrentYear - 2000)
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+1900);
    END;
    ELSE
    BEGIN
        SET @YearOfBirth = SUM(@YearOfBirth+2000);
    END;

	SET @Age = SUM(@CurrentYear- @YearOfBirth); 
    RETURN @Age;
END;
GO


CREATE FUNCTION DBO.[udf_CBU_Gender](
    @Id_Number VARCHAR(13)
)
RETURNS VARCHAR(6)
AS 
BEGIN
	DECLARE @GenderInt INT;
	DECLARE @Gender VARCHAR(6);

	SET @GenderInt = CAST(SUBSTRING(@Id_Number,7,4) AS INT);

	IF @GenderInt < 5000
    BEGIN
        SET @Gender = 'Female';
    END;
    ELSE
    BEGIN
        SET @Gender = 'Male';
    END;
 
    RETURN @Gender;
END;
GO

------ Examples 
SELECT DBO.[udf_CBU_GetAge]('6306034988038') AS Age;
SELECT DBO.[udf_CBU_GetAge]('8612059191789') AS Age;
GO

SELECT DBO.[udf_CBU_Gender]('6306034988038') AS Gender;
SELECT DBO.[udf_CBU_Gender]('8612059191789') AS Gender;
GO































