USE DealerShip
GO

DROP FUNCTION IF EXISTS udf_getListing;
DROP FUNCTION IF EXISTS udf_getListingsByStatus;
GO



CREATE FUNCTION udf_getListing
(   
    @vehicleId INT
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Email_Address,  Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
COALESCE(( SELECT STRING_AGG(Extras.[Name], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Name_Of_Extras,
COALESCE((SELECT STRING_AGG(Extras.[Description], ', ')
	FROM Extras
	RIGHT JOIN Vehicle_Extras ON Vehicle_Extras.Extra_Id = Extras.Extra_Id
	JOIN Vehicles ON Vehicles.Vehicle_Id = Vehicle_Extras.Vehicle_Id
WHERE Vehicle_Extras.Vehicle_Id = Listings.Vehicle_Id ), 'N/A') As Description_Of_Extras,
COALESCE((SELECT STRING_AGG(Vehicle_Images.[Image_Id], ', ')
	FROM Vehicle_Images
WHERE  Vehicle_Images.[Vehicle_Id] = Listings.Vehicle_Id ), 'N/A') As List_Of_Images,

Vehicles.Colour, Vehicles.Engine_Capacity, Vehicles.Fuel_Type, Vehicles.Horsepower, Vehicles.Previous_Owners, Vehicles.Service_History,
Vehicles.Top_Speed, Vehicles.Wheel_Size, Vehicles.[Year], Addresses.Suburb, Addresses.Street_Address
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
WHERE Listings.Vehicle_Id = @vehicleId
);

GO
Select * from udf_getListing(26)
Select * from udf_getListing(44)

GO
CREATE FUNCTION udf_getListingsByStatus
(   
    @status Varchar(10)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT Listings.Title, Vehicles.Model, 
CASE WHEN Vehicles.Automatic_Transmission = 0 
	THEN 'Manual' ELSE 'Automatic' 
END as Transmission, 
CASE WHEN Vehicles.Previous_Owners > 0 
	THEN 'Used' 
	ELSE CASE WHEN Vehicles.Mileage = 0  
		THEN 'New'  ELSE 'Demo' END
END as Condition,
Listings.Price, Business_Units.Phone_Number, 
CASE WHEN Business_Units.Customer_Type = 1 
	THEN 'Dealership' ELSE 'Private Sale' 
END as Customer,
Business_Units.Email_Address, Addresses.Suburb, Addresses.Street_Address, Vehicle_Images.[Image], Packages.[Priority]
FROM Listings
INNER JOIN Business_Units ON Listings.BU_Id = Business_Units.BU_Id
INNER JOIN Vehicles ON Listings.Vehicle_Id = Vehicles.Vehicle_Id
INNER JOIN Addresses ON Business_Units.Address_Id = Addresses.Address_Id
INNER JOIN Vehicle_Images ON Listings.Vehicle_Id = Vehicle_Images.Vehicle_Id
LEFT JOIN Packages ON Listings.Package_Id = Packages.Package_Id
WHERE Listings.Listing_Status = @status
);

GO
Select * from udf_getListingsByStatus('Sold')
Order by Priority desc;
GO
