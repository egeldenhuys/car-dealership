USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Package_Popularity];
GO

CREATE PROCEDURE [usp_Monthly_Package_Popularity]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())
	SELECT DISTINCT pkg.Name, pkg.Description,
					pkg.Price,
					COUNT(lst.Package_Id) OVER (PARTITION BY lst.Package_Id) AS 'Number of Listings',
					COUNT(lst.Package_Id) OVER (PARTITION BY lst.Package_Id) * pkg.Price AS Revenue
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
    WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year
	ORDER BY 'Number of Listings' DESC;
GO