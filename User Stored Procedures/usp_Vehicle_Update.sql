USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Vehicle_Update];
GO

CREATE PROCEDURE [usp_Vehicle_Update](
@Vehicle_Id int,
@Model varchar(20),
@Year int,
@Colour varchar(20),
@Engine_Capacity decimal(4,2),
@Wheel_Size tinyint,
@Mileage int = 0,
@Fuel_Type bit,
@Top_Speed smallint,
@Previous_Owners int = 0,
@Service_History varchar(20),
@Horsepower smallint,
@Vehicle_Vin char(17),
@Automatic_Transmission bit,
@Manufacturer varchar(20),
@Image varbinary(max) = NULL,
@ExtrasList varchar(100) = NULL,
@Type varchar(20) = NULL,
@Carry_Capacity smallint = NULL,
@Shape varchar(20) = NULL,
@Doors tinyint = NULL,
@Body_Type varchar(20) = NULL)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
		
		DECLARE @ManufacturerId int;
		DECLARE @FuelType varchar(20) = 'Petrol'
		DECLARE @VehicleType int = 0;

		IF EXISTS(SELECT * FROM Cars WHERE Vehicle_Id = @Vehicle_Id)
		BEGIN
			SET @VehicleType = 1;
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT * FROM Bikes WHERE Vehicle_Id = @Vehicle_Id)
			BEGIN
				SET @VehicleType = 2;
			END
			ELSE
			BEGIN
				IF EXISTS(SELECT * FROM Trucks WHERE Vehicle_Id = @Vehicle_Id)
				BEGIN
					SET @VehicleType = 3;
				END
			END
		END

		IF @VehicleType NOT IN (1, 2, 3)
		BEGIN
			RAISERROR('Invaild Vehicle Type',16,1);
		END

		SET @ManufacturerId = (SELECT Manufacturer_Id FROM Manufacturers WHERE Name = @Manufacturer);
	
		IF @ManufacturerId IS NULL
		BEGIN 
			INSERT INTO Manufacturers
			(
				Name,
				Email,
				Phone_Number
			)
			VALUES 
			(
				@Manufacturer,
				@Manufacturer+'@company.com',
				'+27145787894'
			)

			SET @ManufacturerId = SCOPE_IDENTITY();

			PRINT 'COMMIT : INSERT INTO Manufacturers'
		END
	
		IF @Fuel_Type = 1
		BEGIN
			SET @FuelType = 'Diesel'
		END
	
		UPDATE Vehicles
		SET Model = @Model,
		Year = @Year,
		Engine_Capacity = @Engine_Capacity,
		Wheel_Size = @Wheel_Size,
		Mileage = @Mileage,
		Fuel_Type = @FuelType,
		Top_Speed = @Top_Speed,
		Previous_Owners = @Previous_Owners,
		Service_History = @Service_History,
		Horsepower = @Horsepower,
		Vehicle_Vin = @Vehicle_Vin,
		Automatic_Transmission = @Automatic_Transmission,
		Manufacturer_Id = @ManufacturerId
		WHERE
		Vehicle_Id = @Vehicle_Id
		
		PRINT 'COMMIT : UPDATE INTO Vehicles'

		--CAR UPDATE
		IF @VehicleType = 1
		BEGIN

			UPDATE Cars
			SET Shape = @Shape,
			Doors = @Doors
			WHERE
			Vehicle_Id = @Vehicle_Id
			
			PRINT 'COMMIT : UPDATE INTO Cars'
			
		END

		--BIKE UPDATE
		IF @VehicleType = 2
		BEGIN

			UPDATE Bikes
			SET Body_Type = Body_Type
			WHERE
			Vehicle_Id = @Vehicle_Id
				
			PRINT 'COMMIT : UPDATE INTO Bikes'

		END

		--TRUCK UPDATE
		IF @VehicleType = 3
		BEGIN
			
			UPDATE Trucks
			SET Type = @Type,
			Carry_Capacity = @Carry_Capacity
			WHERE
			Vehicle_Id = @Vehicle_Id
			
			PRINT 'COMMIT : UPDATE INTO Trucks'
			
		END
		

		IF @Image IS NULL
		BEGIN
			SET @Image = (Select Image from Vehicle_Images where Image_Id = 1);
		END

		UPDATE Vehicle_Images
		SET Image = @Image
		WHERE
		Vehicle_Id = @Vehicle_Id

		PRINT 'COMMIT : UPDATE INTO Image'

		IF @ExtrasList IS NOT NULL
		BEGIN

			DELETE FROM Vehicle_Extras
			WHERE Vehicle_Id = @Vehicle_Id

			INSERT INTO Vehicle_Extras
			SELECT Value as Extra_Id, @Vehicle_Id as Vehicle_Id
			from udf_Split_String(@ExtrasList,',')

			PRINT 'COMMIT : UPDATE INTO Vehicle_Extras'
			
		END

		COMMIT TRANSACTION 
		print 'Transaction committed : Updated Vehicle ' + CAST(@Vehicle_Id AS VARCHAR)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT 'ERROR : ' + ERROR_MESSAGE()
	END CATCH

END
GO
