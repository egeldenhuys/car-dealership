USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Vehicle_Create];
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [usp_Vehicle_Create](
@Model varchar(20),
@Year int,
@Colour varchar(20),
@Engine_Capacity decimal(4,2),
@Wheel_Size tinyint,
@Mileage int = 0,
@Fuel_Type bit,
@Top_Speed smallint,
@Previous_Owners int = 0,
@Service_History varchar(20),
@Horsepower smallint,
@Vehicle_Vin char(17),
@Automatic_Transmission bit,
@Manufacturer varchar(20),
@Image varbinary(max) = NULL,
@VehicleType int,
@ExtrasList varchar(100) = NULL,
@Type varchar(20) = NULL,
@Carry_Capacity smallint = NULL,
@Shape varchar(20) = NULL,
@Doors tinyint = NULL,
@Body_Type varchar(20) = NULL)
AS
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY
		
		IF @VehicleType NOT IN (1, 2, 3)
		BEGIN
			RAISERROR('Invaild Vehicle Type',16,1);
		END

		DECLARE @ManufacturerId int;
		DECLARE @FuelType varchar(20) = 'Petrol'
		DECLARE @VehicleId int;

		DECLARE @extras TABLE (
		Extra_Id INT NOT NULL,
		Vehicle_Id Int NOT NULL);

		SET @ManufacturerId = (SELECT Manufacturer_Id FROM Manufacturers
		WHERE Name = @Manufacturer);
	
		IF @ManufacturerId IS NULL
		BEGIN 
			INSERT INTO Manufacturers
			(
				Name,
				Email,
				Phone_Number
			)
			VALUES 
			(
				@Manufacturer,
				@Manufacturer+'@company.com', --validate Email Function
				'+27145787894'
			)

			SET @ManufacturerId = SCOPE_IDENTITY();

			PRINT 'COMMIT : INSERT INTO Manufacturers'

		END
	
		IF @Fuel_Type = 1
		BEGIN
			SET @FuelType = 'Diesel'
		END
	
		
		INSERT INTO Vehicles
		(
			Model,
			Year,
			Colour,
			Engine_Capacity,
			Wheel_Size,
			Mileage,
			Fuel_Type,
			Top_Speed,
			Previous_Owners,
			Service_History,
			Horsepower,
			Vehicle_Vin,
			Automatic_Transmission,
			Manufacturer_Id
		)
		VALUES
		(
			@Model,
			@Year,
			@Colour,
			@Engine_Capacity,
			@Wheel_Size,
			@Mileage,
			@FuelType,
			@Top_Speed,
			@Previous_Owners,
			@Service_History,
			@Horsepower,
			@Vehicle_Vin,
			@Automatic_Transmission,
			@ManufacturerId
		)

		PRINT 'COMMIT : INSERT INTO Vehicles'

		SELECT @VehicleId = SCOPE_IDENTITY();

		--CAR INSERT
		IF @VehicleType = 1
		BEGIN
		
			INSERT INTO Cars
			(
				Vehicle_Id,
				Shape,
				Doors
			)
			VALUES
			(
				@VehicleId,
				@Shape,
				@Doors
			)
			
			PRINT 'COMMIT : INSERT INTO Cars'
			
		END

		--BIKE INSERT
		IF @VehicleType = 2
		BEGIN

			INSERT INTO Bikes
			(
				Vehicle_Id,
				Body_Type
			)
			VALUES
			(
				@VehicleId,
				@Body_Type
			)
				
			PRINT 'COMMIT : INSERT INTO Bikes'

		END

		--TRUCK INSERT
		IF @VehicleType = 3
		BEGIN
			
			INSERT INTO Trucks
			(
				Vehicle_Id,
				Type,
				Carry_Capacity
			)
			VALUES
			(
				@VehicleId,
				@Type,
				@Carry_Capacity
			)
			
			PRINT 'COMMIT : INSERT INTO Trucks'
			
		END
		

		IF @Image IS NULL
		BEGIN
			SET @Image = (Select Image from Vehicle_Images where Image_Id = 1);
		END

		INSERT INTO Vehicle_Images
		(
			Vehicle_Id,
			Image
		)
		VALUES
		(
			@VehicleId,
			@Image
		)

		PRINT 'COMMIT : INSERT INTO Image'
		
		IF @ExtrasList IS NOT NULL
		BEGIN

			INSERT INTO Vehicle_Extras
			SELECT Value as Extra_Id, @VehicleId as Vehicle_Id
			from udf_Split_String(@ExtrasList,',')

			PRINT 'COMMIT : INSERT INTO Vehicle_Extras'
			
		END

		COMMIT TRANSACTION 
		print 'Transaction committed : Added Vehicle ' + CAST(@VehicleId AS VARCHAR)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT 'ERROR : ' + ERROR_MESSAGE()
	END CATCH

END
GO
