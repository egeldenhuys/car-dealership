USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_Addresses_Insert]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Select]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Update]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_Delete]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_OrphanRecords]; 
DROP PROCEDURE IF EXISTS [usp_Addresses_DeleteOrphanRecords]; 
GO

CREATE PROCEDURE [usp_Addresses_Insert]
	@Address_Id INT OUTPUT,
    @Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;
	
	INSERT INTO [Addresses] ([Street_Address], [Suburb], [PostalCode], [Country])
	SELECT @Street_Address, @Suburb, @PostalCode, @Country;
	
	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country]
	FROM   [Addresses]
	WHERE  [Address_Id] = SCOPE_IDENTITY();
	
	SET @Address_Id = SCOPE_IDENTITY();
	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Select] 
    @Address_Id INT
AS 
	SET NOCOUNT ON; 

	BEGIN TRANSACTION;

	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country] 
	FROM   [Addresses] 
	WHERE  [Address_Id] = @Address_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Update]
    @Address_Id INT,
    @Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30)
AS 
	SET NOCOUNT ON;  
	
	BEGIN TRANSACTION;

	UPDATE [Addresses]
	SET    [Street_Address] = @Street_Address, [Suburb] = @Suburb, [PostalCode] = @PostalCode, [Country] = @Country
	WHERE  [Address_Id] = @Address_Id;
	
	SELECT [Address_Id], [Street_Address], [Suburb], [PostalCode], [Country]
	FROM   [Addresses]
	WHERE  [Address_Id] = @Address_Id;	
	
	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_Delete] 
    @Address_Id INT
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE
	FROM   [Addresses]
	WHERE  [Address_Id] = @Address_Id;

	COMMIT;
GO


CREATE PROCEDURE [usp_Addresses_OrphanRecords] 
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	SELECT	*
	FROM [Addresses] AS a
	LEFT JOIN [Business_Units] AS b ON a.Address_Id = b.Address_Id
	WHERE b.Address_Id IS NULL

	COMMIT;
GO

CREATE PROCEDURE [usp_Addresses_DeleteOrphanRecords] 
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE a
	FROM [Addresses] AS a
	LEFT JOIN [Business_Units] AS b ON a.Address_Id = b.Address_Id
	WHERE b.Address_Id IS NULL

	COMMIT;
GO

-------------------- Tests
-----------
DECLARE	@Address_Id int

EXEC	[dbo].[usp_Addresses_Insert]
		@Address_Id = @Address_Id OUTPUT,
		@Street_Address = N'50 Fisk Plaza',
		@Suburb = N'Dorp',
		@PostalCode = 8560,
		@Country = N'South Africa'

EXEC	[dbo].[usp_Addresses_Select]
		@Address_Id

EXEC	[dbo].[usp_Addresses_Update]
		@Address_Id,
		@Street_Address = N'75 Fisk Plaza',
		@Suburb = N'Dorpie',
		@PostalCode = 8560,
		@Country = N'South Africa'

EXEC	[dbo].[usp_Addresses_Delete]
		@Address_Id

EXEC	[dbo].[usp_Addresses_Select]
		@Address_Id

EXEC	[dbo].[usp_Addresses_OrphanRecords]

EXEC	[usp_Addresses_DeleteOrphanRecords]

EXEC	[dbo].[usp_Addresses_OrphanRecords]

GO
