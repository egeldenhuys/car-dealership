USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Manufacturer_Vehicles_Uploaded];
GO
CREATE PROCEDURE [usp_Monthly_Manufacturer_Vehicles_Uploaded]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())		
SELECT DISTINCT mfr.Name, COUNT(lst.Listing_Id) OVER (PARTITION BY mfr.Manufacturer_Id) AS 'Vehicles Uploaded'
FROM Listings AS lst 
INNER JOIN Vehicles AS vhc
	ON lst.Vehicle_Id = vhc.Vehicle_Id
LEFT JOIN Manufacturers AS mfr
	ON vhc.Manufacturer_Id = mfr.Manufacturer_Id
WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year		
ORDER BY 'Vehicles Uploaded' DESC;
GO