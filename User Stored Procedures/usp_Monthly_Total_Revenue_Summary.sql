USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Total_Revenue_Summary];
GO

CREATE PROCEDURE [usp_Monthly_Total_Revenue_Summary]
	@Year int = null
AS
	IF @Year is null
		SET @Year = YEAR(GETDATE())	
	SELECT DISTINCT DATENAME(month, lst.Date_Created) AS Month,
			SUM(pkg.Price) OVER (PARTITION BY MONTH(lst.Date_Created)) AS 'Total Revenue'
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
	WHERE YEAR(lst.Date_Created) = @Year ;	    
GO