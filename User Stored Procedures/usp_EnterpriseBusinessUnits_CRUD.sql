USE [Dealership];
GO

DROP PROCEDURE IF EXISTS [usp_EBU_Insert];
DROP PROCEDURE IF EXISTS [usp_EBU_Retrieve]; 
DROP PROCEDURE IF EXISTS [usp_EBU_Update]; 
DROP PROCEDURE IF EXISTS [usp_EBU_Delete]; 
GO

CREATE PROCEDURE [usp_EBU_Insert] 
	@BU_Id INT ,
    @Enterprise_Name VARCHAR(30),
    @Enterprise_Number VARCHAR(14)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	INSERT INTO [Enterprise_Business_Units] ([BU_Id], [Enterprise_Name], [Enterprise_Number])
	SELECT @BU_Id, @Enterprise_Name, @Enterprise_Number;
	
	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number]
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;
	           
	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Retrieve] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 

	BEGIN TRANSACTION;

	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number] 
	FROM   [Enterprise_Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Update] 
    @BU_Id INT,
    @Enterprise_Name VARCHAR(30),
    @Enterprise_Number VARCHAR(14)
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	UPDATE [Enterprise_Business_Units]
	SET    [Enterprise_Name] = @Enterprise_Name, [Enterprise_Number] = @Enterprise_Number
	WHERE  [BU_Id] = @BU_Id;
	
	SELECT [BU_Id], [Enterprise_Name], [Enterprise_Number]
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;	
	
	COMMIT;
GO


CREATE PROCEDURE [usp_EBU_Delete] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	
	BEGIN TRANSACTION;

	DELETE
	FROM   [Enterprise_Business_Units]
	WHERE  [BU_Id] = @BU_Id;

	COMMIT;
GO


-------------------- Tests
----------
DECLARE	@BU_Id int
SET @BU_Id = 100

EXEC	[dbo].[usp_EBU_Retrieve]
		@BU_Id

EXEC	[dbo].[usp_EBU_Delete]
		@BU_Id

EXEC	[dbo].[usp_EBU_Retrieve]
		@BU_Id

EXEC	[dbo].[usp_EBU_Insert]
		@BU_Id,
		@Enterprise_Name = N'BBD',
		@Enterprise_Number = N'B1234567891234'

EXEC	[dbo].[usp_EBU_Update]
		@BU_Id,
		@Enterprise_Name = N'Kazu',
		@Enterprise_Number = N'C5579367520489'

GO
