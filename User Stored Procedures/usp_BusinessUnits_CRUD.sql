USE [Dealership]

DROP PROCEDURE IF EXISTS [usp_BU_Retrieve];
DROP PROCEDURE IF EXISTS [usp_BU_Insert];
DROP PROCEDURE IF EXISTS [usp_BU_Update];
GO


CREATE PROCEDURE [usp_BU_Retrieve]
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	SET XACT_ABORT ON

	BEGIN TRANSACTION;

	SELECT [BU_Id], [Phone_Number], [Email_Address], [Address_Id], [Customer_Type]
	FROM   [Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO


CREATE PROCEDURE [usp_BU_Insert] (
	@BU_Id int OUTPUT,
	@Phone_Number varchar(16),
	@Email_Address varchar(50),
	@Address_Id int,
	@Customer_Type smallint)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION

	INSERT INTO Business_Units([Phone_Number], [Email_Address], [Address_Id], [Customer_Type])
	VALUES (@Phone_Number, @Email_Address, @Address_Id, @Customer_Type)
	SET @BU_Id = SCOPE_IDENTITY();
	COMMIT TRANSACTION
GO


CREATE PROCEDURE [usp_BU_Update] (
	@BU_Id int,
	@Phone_Number varchar(16),
	@Email_Address varchar(50))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	UPDATE Business_Units
	SET [Phone_Number] = @Phone_Number,
		[Email_Address] = @Email_Address
	WHERE [BU_Id] = @BU_Id

	COMMIT
GO
