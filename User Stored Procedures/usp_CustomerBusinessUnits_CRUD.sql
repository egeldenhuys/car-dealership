USE [Dealership]

DROP PROCEDURE IF EXISTS [usp_CBU_Insert];
DROP PROCEDURE IF EXISTS [usp_CBU_Retrieve];
DROP PROCEDURE IF EXISTS [usp_CBU_Update];
GO


CREATE PROCEDURE [usp_CBU_Insert] (
	@BU_Id int OUTPUT,
	@Phone_Number varchar(16),
	@Email_Address varchar(50),
	@First_Names varchar(255),
	@Last_Name varchar(255),
	@Id_Number varchar(13),
	@Street_Address VARCHAR(255),
    @Suburb VARCHAR(30),
    @PostalCode INT,
    @Country VARCHAR(30))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	DECLARE @Customer_Type int = 0 -- Customer
	DECLARE @Address_Id int

	EXECUTE usp_Addresses_Insert @Address_Id OUTPUT, @Street_Address, @Suburb, @PostalCode, @Country
	EXECUTE usp_BU_Insert @BU_Id OUTPUT, @Phone_Number, @Email_Address, @Address_Id, @Customer_Type
	
	INSERT INTO Customer_Business_Units([BU_Id], [First_Names], [Last_Name], [Id_Number])
	VALUES(@BU_Id, @First_Names, @Last_Name, @Id_Number)

	COMMIT
GO

-- Example
DECLARE @BU_Id int
EXECUTE usp_CBU_Insert
@BU_Id OUTPUT,
@Phone_Number='0768464875',
@Email_Address='john@gmail.com',
@First_Names='John',
@Last_Name='Bread',
@Id_Number='7811080719547',
@Street_Address='807 Bezuidenhout St',
@Suburb='Secunda',
@PostalCode=2301,
@Country='South Africa'
SELECT @BU_Id as BU_Id
GO

/*
EXECUTE [usp_CBU_Retrieve] 103
GO
*/

SELECT * FROM view_CustomerBusinessUnits
WHERE BU_Id = 103
GO

CREATE PROCEDURE [usp_CBU_Retrieve] 
    @BU_Id INT
AS 
	SET NOCOUNT ON; 
	SET XACT_ABORT ON

	BEGIN TRANSACTION;

	SELECT [BU_Id], [First_Names], [Last_Name], [Id_Number]
	FROM   [Customer_Business_Units] 
	WHERE  [BU_Id] = @BU_Id; 

	COMMIT;
GO

EXECUTE [usp_CBU_Retrieve] 45
GO

CREATE PROCEDURE [usp_CBU_Update] (
	@BU_Id int,
	@First_Names varchar(255),
	@Last_Name varchar(255),
	@Id_Number varchar(13))
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	BEGIN TRANSACTION
	UPDATE Customer_Business_Units
	SET [First_Names] = IsNull(@First_Names, [First_Names]),
		[Last_Name] = IsNull(@Last_Name, [Last_Name]),
		[Id_Number] = IsNull(@Id_Number, [Id_Number])
	WHERE [BU_Id] = @BU_Id
	COMMIT
GO

EXECUTE [usp_CBU_Retrieve] 45
GO

EXECUTE [usp_CBU_Update] @BU_Id = 45, @First_Names=NULL, @Last_Name='Lewis', @Id_Number=NULL
GO

EXECUTE [usp_CBU_Retrieve] 45
GO
