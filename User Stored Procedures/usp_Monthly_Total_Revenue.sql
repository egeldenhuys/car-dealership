USE [Dealership]
GO

DROP PROCEDURE IF EXISTS [usp_Monthly_Total_Revenue];
GO

CREATE PROCEDURE [usp_Monthly_Total_Revenue]
	@Month int = null,
	@Year int = null
AS
	IF @Month is null
		SET @Month = MONTH(GETDATE())
	IF @Year is null
		SET @Year = YEAR(GETDATE())
	SELECT  SUM(pkg.Price) AS 'Month Revenue'
	FROM Packages AS pkg
	LEFT JOIN Listings AS lst
		ON lst.Package_Id = pkg.Package_Id
    WHERE MONTH(Date_Created) = @Month AND YEAR(Date_Created) = @Year ;	
GO